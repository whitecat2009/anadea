function RequestView (el) {
	this.render = function (item) {
		var noteTpl = _.template('<div class="request-box">\
										<img src="<%= user.img %>">\
										<div class="request-content">\
											<div class="request-header">\
												<span class="user-name">\
													<%= user.name %>\
												</span>\
												<span class="request-text"> asks for\
													<%= requestText %>\
												</span>\
												<span class="request-data">\
													<%= requestData %>\
												</span>\
												<span class="request-status <%= status %>">\
													<%= status %>\
												</span>\
											</div>\
											<ul class="comment-box">\
												<% for (var i = 0; i < comments.length; i++)\
													{ %> <li>\
															<span class="comment-txt"><%= comments[i]["commentText"] %></span> - \
															<span class="user-name"><%= comments[i]["user"]["name"] %> </span>\
															<span class="comment-date"><%= comments[i]["date"] %> </span>\
														</li>\
												<% } %>\
											</ul>\
										</div>\
										<div class="user-box">\
											<img src="<%= user.img %>">\
											<span class="user-name">\
												<%= user.name %>\
											</span>\
											<span class="user-role">\
												<%= user.role %>\
											</span>\
										</div>\
									</div>');
		el.innerHTML = el.innerHTML + noteTpl(item);
	};

	return this;
}