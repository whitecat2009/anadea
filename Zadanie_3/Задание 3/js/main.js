$(window).on('load', function () {
	function Main () {
		var itemBox = document.getElementById('board-content'),        
			requestView = new RequestView(itemBox),
			user1 = new UserModel ({
					"name": "Christina Frerichs",
					"img": "img/avatar3.jpg",
					"role": "manager" 
				}),
			user2 = new UserModel ({
					"name": "Harold Henriquez",
					"img": "img/avatar4.jpg" 
				}),
			user3 = new UserModel ({
					"name": "Daniele Longo",
					"img": "img/avatar2.jpg" 
				}),
			user4 = new UserModel ({
					"name": "Harold Henriquez",
					"img": "img/avatar1.jpg" 
				}),
			user1Request = new RequestModel (user1, 'new purchase', 'Notebook, 1000$');
			user2Request = new RequestModel (user2, 'new purchase', 'Notebook, 1000$');
			user3Request = new RequestModel (user3, 'time off', 'May 15 - 20, 5 days. Holiday');
			user4Request = new RequestModel (user4, 'new purchase', 'Notebook, 1000$');
		
		user1Request.addComment('Done', new UserModel({'name': 'Shona L.Brown'}), new Date());
		user1Request.addComment('I want a new laptop', user1, new Date());
		user2Request.addComment('Done', user1, new Date());
		user3Request.addComment('Ok, I don\'t mind', new UserModel({'name': 'Shona L.Brown'}), new Date());
		user4Request.addComment('I want a new laptop', user4, new Date());

		user2Request.status = 'approved';
		user3Request.status = 'bought';


		this.init = function () {
			requestView.render(user1Request);
			requestView.render(user2Request);
			requestView.render(user3Request);
			requestView.render(user4Request);
		};

		return this;
		
	}

	var main = new Main();

	main.init();

	$(document)
		.on('click', '.user-name, .request-box img', function () {
			$(this).closest('.request-box').find('.user-box').show();
		})
		.on('mouseleave', '.user-box', function () {
			$('.user-box').hide();
		});
});

