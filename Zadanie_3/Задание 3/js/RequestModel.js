function RequestModel (user, requestText, requestData) {
	this.comments = [];
	this.status = 'pending';
	this.user = user;
	this.requestText = requestText;
	this.requestData = requestData;
	
	this.addComment = function (commentText, user, date) {
		this.comments.push({commentText: commentText, user: user, date: date.toDateString()});
	}
	
	this.getLastComment = function () {
		return this.comments[this.comments.length - 1];
	}
	this.getAllComments = function () {
		return this.comments;
	}
	return this;
}